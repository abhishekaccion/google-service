import { EventEmitter as events } from "events";
import * as passport from "passport";
import { OAuth2Strategy as GoogleStrategy } from "passport-google-oauth";
import { config } from "../../config";
import { Credentials } from "../types";

export class GoogleAuth extends events {
  clientID: string;
  clientSecret: string;
  callbackURL: string;
  scope: Array<string>;

  constructor() {
    super();
  }

  public initialize(creds: Credentials) {
    this.clientID = creds.clientID;
    this.clientSecret = creds.clientSecret;
    this.callbackURL = creds.callbackURL;
    this.scope = creds.scope;

    return passport.initialize();
  }

  public strategy() {
    // serialize and deserialize
    passport.serializeUser((user, done) => {
      done(null, user);
    });
    passport.deserializeUser((obj, done) => {
      done(null, obj);
    });

    passport.use(
      new GoogleStrategy(
        {
          clientID: this.clientID,
          clientSecret: this.clientSecret,
          callbackURL: this.callbackURL,
          scope: this.scope
        },
        (accessToken, refreshToken, profile, done) => {
          done(null, profile);
        }
      )
    );
  }

  public authenticate() {
    return passport.authenticate("google");
  }
}
