import { EventEmitter as events } from "events";
import { config } from "../../config";
import { Credentials } from "../types";
import { fetch } from "node-fetch";

export class Message extends events {
  labels: any = ["IMPORTANT", "INBOX"];

  constructor() {
    super();
  }

  private get(url, headers) {
    return fetch(url, { method: "GET", headers: headers })
      .then(res => res.json())
      .then(body => body)
      .catch(error => {
        throw new Error(error.message);
      });
  }

  async label(token) {
    const headers = {
      Authorization: `Bearer ${token}`
    };
    return new Promise(async (resolve, reject) => {
      try {
        const result = await this.get(
          `https://www.googleapis.com/gmail/v1/users/me/labels`,
          headers
        );

        return resolve(result);
      } catch (error) {
        return reject(error.message);
      }
    });
  }

  async list(token, requestLabels, maxResults = 10) {
    const headers = {
      Authorization: `Bearer ${token}`
    };
    return new Promise(async (resolve, reject) => {
      try {
        // labels
        if (!!requestLabels && typeof requestLabels === "string") {
          this.labels = [requestLabels];
        }
        if (!!requestLabels) {
          this.labels = requestLabels;
        }
        this.labels = this.labels.map(label => `labelIds=${label}`);
        this.labels = this.labels.join("&");
        const result = await this.get(
          `https://www.googleapis.com/gmail/v1/users/me/messages?${
            this.labels
          }&maxResults=${maxResults}`,
          headers
        );
        return resolve(result);
      } catch (error) {
        return reject(error.message);
      }
    });
  }

  private async getMessageList(
    token: string,
    requestLabels: any,
    maxResults: number
  ) {
    const headers = {
      Authorization: `Bearer ${token}`
    };
    try {
      // labelsIds
      let labelIds: any = await this.list(token, requestLabels, maxResults);
      labelIds = labelIds.messages;
      let messagelist: any = await Promise.all(
        labelIds.map(async labelId => {
          return await this.get(
            `https://www.googleapis.com/gmail/v1/users/me/messages/${labelId}`,
            headers
          );
        })
      );
      return messagelist;
    } catch (error) {
      return error.message;
    }
  }

  async message(token: string, request: any, callback: any) {
    const headers = {
      Authorization: `Bearer ${token}`
    };
    let messageId: string = request.messageId ? request.messageId : false;
    let requestLabels = request.requestLabels ? request.requestLabels : null;
    let maxResults = request.maxResults ? request.maxResults : 10;

    return new Promise(async (resolve, reject) => {
      try {
        let result = null;

        if (messageId) {
          result = await this.get(
            `https://www.googleapis.com/gmail/v1/users/me/messages/${messageId}`,
            headers
          );
        } else {
          result = await this.getMessageList(token, requestLabels, maxResults);
        }
        return resolve(result);
      } catch (error) {
        return reject(error.message);
      }
    });
  }
}
