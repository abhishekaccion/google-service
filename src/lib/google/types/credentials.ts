export interface Credentials {
  clientID: string;
  clientSecret: string;
  callbackURL: string;
  scope: Array<string>;
}
